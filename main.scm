
;; guile's definition
(define-syntax define-syntax-rule
  (lambda (x)
    (syntax-case x ()
      ((_ (name . pattern) template)
       #'(define-syntax name
           (syntax-rules ()
             ((_ . pattern) template)))))))

(define (return x) x)

(define (log2 x)
  (/ (log x) (log 2)))
(define (ceil x)
  (inexact->exact (ceiling x)))
(define (floo x)
  (inexact->exact (floor x)))

(define %metric-t 0)
(define %metric-w 0)
(define %metric-p 1)

(define (speedup)
  (/ %metric-w %metric-t))
(define (processor-efficiency)
  (/ (speedup) %metric-p))

(define (display-metrics)
  (display "t ")
  (display %metric-t)
  (display " | w ")
  (display %metric-w)
  (display " | p ")
  (display %metric-p)
  (display " | E ")
  (display (exact->inexact (processor-efficiency)))
  (newline))

(define (reset-metrics!)
  (set! %metric-t 0)
  (set! %metric-w 0)
  (set! %metric-p 1))

(define scope-lst (list (gensym)))
(define last-scope-car (make-parameter (box (gensym))))

(define (reset-all!)
  (reset-metrics!)
  (set! scope-lst (list (gensym)))
  (set! last-scope-car (make-parameter (box (gensym)))))

(define-syntax-rule (scope-param type . bodies)
  (parameterize ((last-scope-car (box (gensym))))
    (begin . bodies)))

(define-syntax-rule (scope-do . bodies)
  (begin
    (set! scope-lst (cons (gensym) scope-lst))
    (let ((result (let () . bodies)))
      (set! scope-lst (cdr scope-lst))
      result)))

(define-syntax-rule (t++ . bodies)
  (let ((b (last-scope-car)))
    (unless (eq? (car scope-lst) (unbox b))
      (set-box! b (car scope-lst))
      (set! %metric-t (+ 1 %metric-t))
      (set! %metric-w (+ 1 %metric-w)))
    . bodies))

(define-syntax iff
  (syntax-rules ()
    ((_ test then else)
     (t++ (if test then else)))
    ((_ test then)
     (t++ (when test then)))))

(define (vector-print v)
  (define matrix? (vector? (vector-ref v 0)))
  (let ((len (vector-length v)))
    (if matrix?
        (let lp ((i 0))
          (when (< i len)
            (let* ((vv (vector-ref v i))
                   (len2 (vector-length vv)))
              (display "(")
              (let jp ((j 0))
                (when (< j len2)
                  (display (vector-ref vv j))
                  (when (< j (- len2 1))
                    (display " "))
                  (jp (+ 1 j))))
              (display ")"))
            (display "\n")
            (lp (+ 1 i))))
        (begin
          (display v)
          (newline)))))

(define vector-fill 0)
(define default-value 0)

(define (make-vector-of sizes)
  (case (length sizes)
    ((0) default-value)
    ((1) (make-vector (car sizes) vector-fill))
    (else
     (let ((v (make-vector (car sizes))))
       (let lp ((i (- (car sizes) 1)))
         (unless (< i 0)
           (vector-set! v i (make-vector-of (cdr sizes)))
           (lp (- i 1))))
       v))))

(define-syntax-rule (declare name . sizes)
  (define name (make-vector-of (list . sizes))))

(define-syntax declare*-helper
  (syntax-rules ()
    ((_ buf ())
     (begin . buf))
    ((_ buf (x . xs))
     (declare*-helper ((declare x) . buf) xs))))
;; (define-macro (declare* . lst)
;;   (cons* 'begin (map (lambda (x) (list 'declare x)) lst)))
(define-syntax-rule (declare* . lst)
  (declare*-helper () lst))

(define-syntax-rule (ref x)
  (- x 1))

(define-syntax rr-helper
  (syntax-rules ()
    ((_ H (i))
     (t++ (vector-ref H (ref i))))
    ((_ H (x . xs))
     (rr-helper (vector-ref H (ref x)) xs))))
(define-syntax-rule (rr H . indexes)
  (rr-helper H indexes))

(define-syntax :=-helper
  (syntax-rules ()
    ((_ H (v))
     (t++ (set! H v)))
    ((_ H (i v))
     (t++ (vector-set! H (ref i) v)))
    ((_ H (x . xs))
     (:=-helper (vector-ref H (ref x)) xs))))
(define-syntax-rule (:= H . indexes)
  (:=-helper H indexes))

(define-syntax-rule (while test . bodies)
  (scope-param
   scope-type-dodo
   (let* ((p0 %metric-p)
          (p-max p0))
     (let lp ()
       (when test
         (set! %metric-p p0)
         (scope-do . bodies)
         (set! p-max (max p-max %metric-p))
         (lp)))
     (set! %metric-p p-max))))

(define-syntax-rule (dodo i start end . bodies)
  (scope-param
   scope-type-dodo
   (let* ((e end)
          (p0 %metric-p)
          (p-max p0))
     (let lp ((i start))
       (unless (> i e)
         (set! %metric-p p0)
         (scope-do . bodies)
         (set! p-max (max p-max %metric-p))
         (lp (+ 1 i))))
     (set! %metric-p p-max))))

(define-syntax-rule (pardo i start end . bodies)
  (scope-param
   scope-type-pardo
   (let ((e end)
         (t0 %metric-t)
         (t-max %metric-t)
         (p0 %metric-p)
         (p-max 1))
     (let lp ((i start))
       (unless (> i e)
         (set! %metric-t t0)
         (set! %metric-p 1)
         (scope-do . bodies)
         (set! t-max (max t-max %metric-t))
         (set! p-max (max p-max %metric-p))
         (lp (+ 1 i))))
     (set! %metric-t t-max)
     (set! %metric-p (+ p0 (* p-max (+ 1 (- end start))))))))

(define-syntax-rule (parallel-h . bodies)
  (scope-param
   scope-type-pardo
   (let ((t0 %metric-t)
         (t-max %metric-t))
     (for-each
      (lambda (f)
        (set! %metric-t t0)
        (scope-do . (f))
        (set! t-max (max t-max %metric-t))
        (list . bodies))
      (set! %metric-t t-max)))))

(define-syntax parallel-helper
  (syntax-rules ()
    ((_ buf ())
     (parallel-h (reverse (list . buf))))
    ((_ buf (x . xs))
     (parallel-helper ((lambda _ x) . buf) xs))))
(define-syntax-rule (parallel . bodies)
  (parallel-helper () bodies))

(define-syntax-rule (defun (name . args) . bodies)
  (define name
    (let ((recursion? (make-parameter #f)))
      (lambda args
        (if (recursion?)
            (scope-param scope-type-seq (scope-do . bodies))
            (parameterize ((recursion? #t)) . bodies))))))


