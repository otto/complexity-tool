
(use-modules (ice-9 match))
(use-modules (srfi srfi-111))

(define (list-init lst)
  (if (null? lst)
      (list)
      (let lp ((rest lst))
        (if (null? (cdr rest))
            (list)
            (cons (car rest) (lp (cdr rest)))))))
(define (list-last lst)
  (let lp ((rest lst))
    (if (null? (cdr rest))
        (car rest)
        (lp (cdr rest)))))

(define read-list
  (case-lambda
   (() (read-list (current-input-port)))
   ((input)
    (let ((p (if (string? input) (open-input-string input)
                 input)))
      (let lp ()
        (let ((r (read p)))
          (if (eof-object? r)
              (begin
                (when (string? input)
                  (close-port p))
                (list))
              (cons r (lp)))))))))

(define (translate-expr tab inline? expr)
  (define (~a x)
    (with-output-to-string
      (lambda ()
        (display x))))
  (define recur-string
    (lambda (tab)
      (lambda (expr)
        (with-output-to-string
          (lambda _ (translate-expr tab #t expr))))))
  (define (display-inline x)
    (display ((recur-string 0) x)))
  (define (map-ind i)
    (display (string-append "[" ((recur-string 0) i) "]")))
  (define make-tabs
    (case-lambda
     (() (make-tabs tab))
     ((i)
      (unless inline?
        (let lp ((i i))
          (when (> i 0) (display "\t") (lp (1- i))))))))
  (define (endl)
    (unless inline? (newline)))

  (match expr
         (('declare . xs)  #f)
         (('declare* . xs) #f)
         (('display-metrics . xs) #f)
         (('vector-print . xs) #f)
         (('debug . xs) #f)
         (('display . xs) #f)
         (('defun name-args . xs)
          (make-tabs)
          (display (car name-args))
          (display "(")
          (display (string-join (map (recur-string 0) (cdr name-args)) ", "))
          (display "): ")
          (endl)
          (for-each (lambda (x) (translate-expr (1+ tab) inline? x)) xs)
          (endl))
         (('begin . xs)
          (for-each (lambda (x) (translate-expr tab #f x)) xs))
         (('pardo i start end . xs)
          (make-tabs)
          (display "for ")
          (display i)
          (display " in ")
          (display-inline start)
          (display " .. ")
          (display-inline end)
          (display " pardo ")
          (endl)
          (for-each (lambda (x) (translate-expr (1+ tab) inline? x)) xs))
         (('dodo i start end . xs)
          (make-tabs)
          (display "for ")
          (display i)
          (display " in ")
          (display-inline start)
          (display " .. ")
          (display-inline end)
          (display " do ")
          (endl)
          (for-each (lambda (x) (translate-expr (1+ tab) inline? x)) xs))
         (('while test . xs)
          (make-tabs)
          (display "while ")
          (display-inline test)
          (display " do ")
          (endl)
          (for-each (lambda (x) (translate-expr (1+ tab) inline? x)) xs))
         (('iff test then)
          (make-tabs)
          (display "if ")
          (display-inline test)
          (display " then ")
          (endl)
          (translate-expr (1+ tab) inline? then)
          (endl))
         (('iff test then else)
          (make-tabs)
          (display "if ")
          (display-inline test)
          (display " then ")
          (endl)
          (translate-expr (1+ tab) inline? then)
          (newline)
          (make-tabs)
          (display "else ")
          (endl)
          (translate-expr (1+ tab) inline? else)
          (endl))
         ((':= x . is)
          (let ((xs (list-init is))
                (v (list-last is)))
            (make-tabs)
            (display x)
            (for-each map-ind xs)
            (display #\space)
            (display ':=)
            (display #\space)
            (translate-expr 0 #t v)
            (endl)))
         (('rr x . xs)
          (make-tabs)
          (display x)
          (for-each map-ind xs))
         (('ceil x) (translate-expr tab inline? `(ceiling ,x)))
         (('floo x) (translate-expr tab inline? `(floor ,x)))
         (('sub1 x) (translate-expr tab inline? `(- ,x 1)))
         (('add1 x) (translate-expr tab inline? `(+ ,x 1)))
         (('expt x y) (translate-expr tab inline? `(^ ,x ,y)))
         (('remainder x y) (translate-expr tab inline? `(% ,x ,y)))
         (('equal? x y) (translate-expr tab inline? `(= ,x ,y)))
         ((op . xs)
          (make-tabs)
          (case op
            ((+ - * / ^ % = < > <= >=)
             (unless (= 2 (length xs))
               (throw 'only-binary-op-supported expr))
             (display "(")
             (translate-expr 0 #t (car xs))
             (display " ")
             (display op)
             (display " ")
             (translate-expr 0 #t (cadr xs))
             (display ")"))
            (else
             (display (car expr))
             (display "(")
             (display (string-join (map (recur-string 0) (cdr expr)) ", "))
             (display ")")
             (endl))))
         (x (display x))))

(define (translate-file filepath)
  (define in (open-file filepath "r"))
  (define exprs (read-list in))
  (close-port in)
  (for-each (lambda (x) (translate-expr 0 #f x)) exprs))

(translate-file (cadr (command-line)))
